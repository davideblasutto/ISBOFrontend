// ========== Configura bundle ==========
// Stile (a sua volta include i file Less di UIkit)
import './stile.less'
// UIkit
import "uikit"
import "uikit/dist/js/components/notify"
// Altri pacchetti
import Cookies from "js-cookie"
import Clipboard from "clipboard"
const merge = require("merge")
const FileSaver = require("file-saver")
const JSZip = require('jszip')
const JSZipUtils = require('jszip-utils')
const jsmediatags = require('jsmediatags')
require("jquery-deparam")
require("devbridge-autocomplete")
require("./jquery.binarytransport-with-progress.js")

// ========== Variabili globali ==============

if (location.host == "localhost") {
	// Sviluppo
	self.backend = "http://localhost:8080"
	self.delayScreenshot = 3500
	self.urlDefault = "https://wol.jw.org/it/wol/dt/r6/lp-i/2018/2/13"
} else {
	// Produzione
	self.backend = "https://isbo.info.tm"
	self.delayScreenshot = 2750
}

self.config = {}
self.elaborazioneInCorso = false
self.timeoutMessaggioElaborazione = {}
self.versioneBackend = ""
self.versioneFrontend = ""
self.istanteInizioDownload = 0
self.percentuale = 0
self.avviatoIntervalETA = 0
self.lingue = []

// ========== Funzioni =================

//
// ## Ottiene da wol l'elenco delle lingue e mostra l'apposito controllo
//
function caricaLingue() {

	// Passa per il proxy CORS del backend
	$.ajax({
		url: backend + "/cors",
		data: {url: "http://wol.jw.org/it/wol/sysl/r6/lp-i" },
		success: function(risposta){

			// Parsa la pagina creando un'array con codice, nome locale e nome internazionale di ogni lingua
			$($.parseHTML(risposta)).find(".completeList ul.librarySelection li[role='row']").each(function(i, e) {
				var a = $(e).find("a").first()
				lingue.push({
					// La coppia data+value è per jquery-autocomplete
					data: a.attr("data-locale"),
					value: a.find("li").first().find("span").last().text().trim() + " - " + a.find("li").first().find("span").first().text().trim()
				})
			})
			// La lingua in cui viene eseguita la ricerca non compare nell'elenco
			lingue.push({ data: "it", value: "Italiano" })

			// Abilita autocomplete
			$("#lingua").autocomplete({
				width: 450,
				autoSelectFirst: true,
				lookup: lingue,
				onSelect: function (suggestion) {
					$("#lingua").val(suggestion.data).blur()
					$("#spanLingua").text(suggestion.value)
					calcolaConfig()
				}
			});

			// Mostra subito il nome della lingua attuale, se possibile
			modificaInput()

		}
	});

}

//
// ## All'inizio dell'elaborazione aggiorna la GUI di conseguenza
//
function inizioElaborazione() {

	$("#btnElabora i").addClass("uk-icon-spin")
	$("#btnElabora span").html("Elaborazione in corso")
	$("#btnElabora").addClass("uk-button-primary")
	$("#btnElabora").css("cursor", "default")
	$("#btnElabora").removeClass("uk-button-success")
	$(".btnAdunanza").removeClass("uk-button-success")
	elaborazioneInCorso = true
	istanteInizioDownload = 0
	percentuale = 0
	avviatoIntervalETA = false

}

//
// ## Alla fine dell'elaborazione aggiorna la GUI di conseguenza
//
function fineElaborazione() {

	$("#spanEta").parent().addClass("uk-hidden")
	$("#btnElabora i").removeClass("uk-icon-spin")
	$("#btnElabora span").html("Elabora")
	$("#btnElabora").removeClass("uk-button-primary")
	$("#btnElabora").css("cursor", "pointer")
	$("#btnElabora").addClass("uk-button-success")
	$(".btnAdunanza").addClass("uk-button-success")
	elaborazioneInCorso = false
	$(".uk-progress").addClass("uk-invisible")
	$(".uk-progress-bar").css("width", "0%")
	$(".uk-progress-bar").text("")
	clearTimeout(timeoutMessaggioElaborazione)
	istanteInizioDownload = 0
	percentuale = 0

}

//
// ## Gestisce eventuali errori ricevuti dal backend
//
function mostraErrore(messaggio) {

	fineElaborazione()
	console.log("!!! Errore backend")
	console.log(messaggio)

	if (typeof messaggio != "string")
		// Errore generico nella comunicazione con il backend
		UIkit.notify("<i class='uk-icon-exclamation-circle'></i>&nbsp;Impossibile comunicare con i servizi web necessari. Verificare la connessione a Internet. Se il problema persistite <a href='mailto:davide.blasutto@gmail.com'>contattare lo sviluppatore</a>.", "danger");
	else
		// Errore generico nella comunicazione con il backend
		UIkit.notify("<i class='uk-icon-exclamation-circle'></i>&nbsp;Errore nell'elaborazione:<br>" + messaggio, "danger");

}

//
// ## Invia al backend la configurazione e gestisce la risposta
//
function richiediElaborazione(dati) {

	// Se sta già elaborando non fa nulla
	if (elaborazioneInCorso) { return }

	console.log("### Inviata richiesta di elaborazione:")
	console.log(dati)
	inizioElaborazione()

	// Messaggi all'utente durante l'elaborazione
	timeoutMessaggioElaborazione = setTimeout(function() {
		$("#btnElabora span").text("Potrebbe volerci ancora un po'")
		timeoutMessaggioElaborazione = setTimeout(function() {
			$("#btnElabora span").text("Ci siamo quasi...")
			timeoutMessaggioElaborazione = setTimeout(function() {
				$("#btnElabora span").text("Ancora un attimo...")
			}, 60 * 1000)
		}, 40 * 1000)
	}, 20 * 1000)

	// Prepara la richieste, definendo prima i parametri comuni a tutte le richieste...
	var parametriRichiesta = {
		headers: { "Frontend-Web": "true" },
		dataType: "binary",
		timeout: 1800 * 1000, // 30 minuti
		error: mostraErrore,
		progress: function(e) {
			// All'avanzamento
			if (e.lengthComputable) {

				// Calcola percentuale
				percentuale = (e.loaded / e.total) * 100

				// La prima volta segna l'inizio del download
				if (istanteInizioDownload === 0) {
					istanteInizioDownload = performance.now()
					// Evita che partano altri messaggi di attesa
					clearTimeout(timeoutMessaggioElaborazione)
					$("#btnElabora span").text("Download in corso")
				}

				// Aggiorna progress bar
				$(".uk-progress").removeClass("uk-invisible")
				$(".uk-progress-bar").css("width", percentuale + "%")

				// Al 5% mostra l'ETA
				if (parseInt(percentuale) <= 5 && !avviatoIntervalETA) {
					setTimeout(function() {
						if (elaborazioneInCorso)
							$("#spanEta").parent().removeClass("uk-hidden")
					}, 1000)
					avviatoIntervalETA = true
					// Aggiorna l'ETA ogni secondo
					setInterval(function() {
						var secondiTrascorsi = (performance.now() - istanteInizioDownload) / 1000
						var eta = parseInt(secondiTrascorsi * (100 / percentuale) - secondiTrascorsi)
						var m = parseInt(eta / 60)
						var s = parseInt(eta % 60)
						if (s < 10)
							s = "0" + s
						$("#spanEta").text(m + ":" + s)
					}, 1000)
				}

			}
		}
	}

	// ... e poi quelli specifici ("config" o upload di file locali)
	if (typeof dati.origine != "undefined") {

		// Config
		parametriRichiesta.url = backend + "/?" + $.param(dati)
		parametriRichiesta.method = "GET"

	} else {

		// File locali
		parametriRichiesta.data = dati
		parametriRichiesta.url = backend + "/upload"
		parametriRichiesta.method = "POST"
		parametriRichiesta.processData = false
		parametriRichiesta.contentType = false
		parametriRichiesta.mimeType = "multipart/form-data"

	}

	// Esegue la richiesta
	$.ajax(parametriRichiesta).done(function(risposta, stato, xhr) {

		console.log("### Ricevuta risposta")

		switch (xhr.status) {

			case 299:
				// Errore
				// (La richiesta includeva l'header ("Frontend-Web": true), quindi il backend in caso di errore restituisce 299 invece di 4##)
				var readerErrore = new window.FileReader()
				readerErrore.readAsText(risposta)
				readerErrore.onloadend = function() {
					console.log("### Errore - " + readerErrore.result)
					mostraErrore(readerErrore.result)
					fineElaborazione()
				}
				break

			default:
				// OK
				// La risposta è un blob con un archizi zip
				console.log("### OK")

				// Scarica subito o mostra i contenuti?
				if (!config.mostraAnteprima) {

					// Scarica subito
					FileSaver.saveAs(risposta, "Contenuti.zip")
					fineElaborazione()

				} else {

					// Rende la risposta accessibile globalmente, per il download in seguito
					self.risposta = risposta

					// Mostra anteprima
					mostraAnteprima()

				}
				break

		}

	})

}

//
// ## Mostra anteprima dei file contenuti in self.risposta
//
function mostraAnteprima() {

	// Svuota container
	$("#containerAnteprima").html('')

	var readerAnteprima = new window.FileReader()
	readerAnteprima.readAsDataURL(risposta)
	readerAnteprima.onloadend = async function() {

		// Legge il file zip
		var data = await new JSZip.external.Promise(function (resolve, reject) {
			JSZipUtils.getBinaryContent(readerAnteprima.result, function(err, data) {
				if (err) {
					reject(err)
				} else {
					resolve(data)
				}
			})
		})
		var zip = await JSZip.loadAsync(data)
		console.log(zip)

		// Cicla sui file nello zip
		var i = 0
		for (const nomeFile in zip.files) {
			i++
			const file = zip.files[nomeFile]

			var $panel

			// Nello zip possono esserci solo file .png, .url o video
			switch (nomeFile.split('.').pop()) {

				case 'png':
					// Immagine
					$panel = $("#templateAnteprimaImmagine").clone().removeAttr("id").removeClass("uk-hidden")
					$panel.find("img").caricaBlob(await file.async('blob'))
					break

				case 'url':
					// Link
					const contenuto = await file.async('text')
					const url = contenuto.substr(contenuto.indexOf('=') + 1)
					$panel = $("#templateAnteprimaLink").clone().removeAttr("id").removeClass("uk-hidden")
					$panel.caricaScreenshot(url)
					break

				case 'mp4':
					// Video
					$panel = $('#templateAnteprimaVideo').clone().removeAttr('id').removeClass('uk-hidden')
					// Rende il $panel disponibile nella callback qui sotto
					var $panelVideo = $panel
					// Carica illustrazione del video
					jsmediatags.read(await file.async('blob'), {
						onSuccess: function(tag) {
							// Converte tag.tags.picture.data in base64
							var base64String = "";
							for (var i = 0; i < tag.tags.picture.data.length; i++) {
							  	base64String += String.fromCharCode(tag.tags.picture.data[i])
							}
							$panelVideo.find('img')[0].onload = function() {
								// Altezza fissa
								$panelVideo.find('img').height($panelVideo.find('img').width() / config.rapportoImmagini)
							}
							$panelVideo.find('img').attr('src', "data:" + tag.tags.picture.data.format + ";base64," + window.btoa(base64String))
						}
					})
					break

			}

			// Per tutti i tipi di file:
			$panel.find(".uk-panel-title span").text(nomeFile)
			$panel.attr('data-titolo', nomeFile)
			$("#containerAnteprima").append($panel)

		}
		var elementiMostrati = i
		console.log('Mostro ', i, 'elementi')

		// Ordina i panel
		$('#containerAnteprima .elemento').detach().sort((a, b) => {
			return $(a).attr('data-titolo').localeCompare($(b).attr('data-titolo'))
		}).appendTo('#containerAnteprima')

		// Eventuale ridimensionanmento
		if (elementiMostrati == 1)
			// Uno per riga
			$('#containerAnteprima .elemento').removeClass('uk-width-large-1-3 uk-width-medium-1-2').addClass('uk-width-1-1')
		else if (elementiMostrati <= 4)
			// Due per riga
			$('#containerAnteprima .elemento').removeClass('uk-width-large-1-3')

		// Mostra il modal dell'Anteprima
		UIkit.modal("#modalAnteprima", { center: true, bgclose: false }).show()

		// Dopo aver iniziato a mostrare il modal ridimensiona i div di caricamento
		$('.caricamento').height($('.caricamento').width() / config.rapportoImmagini)

		fineElaborazione()

	}

}

//
// ## Carica lo screenshot di una pagina in un "panelLink"
//
$.fn.caricaScreenshot = function(url) {

	var $panel = $(this)

	// Chiede lo screenshot al backend
	$.ajax({
  		url: backend + "/screenshot",
  		data: {
			url: url,
		 	larghezza: 1024,
			rapporto: config.rapportoImmagini,
			delay: delayScreenshot
		},
  		dataType: 'binary'
	}).done(function(blob) {
		$panel.find('.caricamento').addClass('uk-hidden')
		$panel.find('img').removeClass('uk-hidden')
		$panel.find('img').caricaBlob(blob)
  	})

}

//
// ## Carica un blob come src di un elemento
//
$.fn.caricaBlob = function(blob) {

	const $elemento = $(this)

	// Lo converto in base64 per usarlo come src
	var reader = new FileReader()
	reader.readAsDataURL(blob)
	reader.onloadend = function() {
		$elemento.attr('src', reader.result)
	}

}

//
// ## Aggiorna e salva l'oggetto config sulla base degli input di configurazione
//
function calcolaConfig(salva){

	config.lingua = $("#lingua").val()
	config.lunghezzaMaxRiferimenti = $("#lunghezzaMaxScrittura").val()
	config.lunghezzaMaxVideo = $("#lunghezzaMaxVideo").val()
	config.rapportoImmagini = $("#rapportoImmagini .uk-active").attr("data-val")
	config.origine.offset = $("#offset .uk-active").attr("data-val")
	config.sfondo = $("#tipoSfondo .uk-active").attr("data-val")
	config.risoluzioneVideo = $("#risoluzioneVideo .uk-active").attr("data-val")
	config.cerca = []
	$([ "illustrazioni", "link", "riferimenti", "video" ]).each(function(i, tipoContenuto) {
		if ($("#cerca [data-val='" + tipoContenuto + "']").is(".uk-active"))
		config.cerca.push(tipoContenuto)
	})
	config.mostraAnteprima = $("#mostraAnteprima").prop('checked')
	config.dividiImmaginiComposte = $("#dividiImmaginiComposte").prop('checked')

	// Aggiorna la gui
	mostraConfig()
	// Salva nei cookie
	Cookies.set("isbo", config, { expires: 365 })

}

//
// ## Aggiorna la GUI sulla base di config
//
function mostraConfig(){

	// Lingua
	$("#lingua").val(config.lingua);

	// Lunghezza max riferimenti (mostra se necessario)
	$("#lunghezzaMaxScrittura").val(config.lunghezzaMaxRiferimenti);
	if (config.cerca.indexOf("riferimenti") != -1)
		$("#lunghezzaMaxScrittura").closest(".uk-form-row").removeClass("uk-hidden")
	else
		$("#lunghezzaMaxScrittura").closest(".uk-form-row").addClass("uk-hidden")

	// Lunghezza max video (mostra se necessario)
	$("#lunghezzaMaxVideo").val(config.lunghezzaMaxVideo);
	if (config.cerca.indexOf("video") != -1)
		$("#lunghezzaMaxVideo").closest(".uk-form-row").removeClass("uk-hidden")
	else
		$("#lunghezzaMaxVideo").closest(".uk-form-row").addClass("uk-hidden")

	// Rapporto immagini
	$("#rapportoImmagini button").removeClass("uk-active uk-button-primary");
	$("#rapportoImmagini").find("[data-val='" + config.rapportoImmagini + "']").addClass("uk-active uk-button-primary");

	// Tipo sfondo (mostra se necessario)
	$("#tipoSfondo button").removeClass("uk-active uk-button-primary");
	$("#tipoSfondo").find("[data-val=" + config.sfondo + "]").addClass("uk-active uk-button-primary");
	if (config.cerca.indexOf("riferimenti") != -1)
		$("#tipoSfondo").closest(".uk-form-row").removeClass("uk-hidden")
	else
		$("#tipoSfondo").closest(".uk-form-row").addClass("uk-hidden")

	// Risoluzione video (mostra se necessario)
	$("#risoluzioneVideo button").removeClass("uk-active uk-button-primary");
	$("#risoluzioneVideo").find("[data-val='" + config.risoluzioneVideo + "']").addClass("uk-active uk-button-primary");
	if (config.cerca.indexOf("video") != -1)
		$("#risoluzioneVideo").closest(".uk-form-row").removeClass("uk-hidden")
	else
		$("#risoluzioneVideo").closest(".uk-form-row").addClass("uk-hidden")

	// Tipi di contenuti da estrarre
	$("#cerca button").removeClass("uk-active uk-button-primary");
	$(config.cerca).each(function(i, tipoContenuto) {
		$("#cerca").find("[data-val=" + tipoContenuto + "]").addClass("uk-active uk-button-primary");
	})

	// Risoluzione video
	$("#offset button").removeClass("uk-active uk-button-primary");
	$("#offset").find("[data-val='" + config.origine.offset + "']").addClass("uk-active uk-button-primary");

	// Anteprima
	$("#mostraAnteprima").prop('checked', config.mostraAnteprima)

	// Immagini composte
	$("#dividiImmaginiComposte").prop('checked', config.dividiImmaginiComposte)

}

// ========== Eventi ===========

//
// ## Click sul pulsantl "Scarica" (in modal anteprima contenuti)
//
$(document).on("click", "#btnScarica", function(){

	FileSaver.saveAs(risposta, "Contenuti.zip")
	UIkit.modal("#modalAnteprima").hide()

})

//
// ## Click sui pulsanti "Rapporto immagini", "Tipo sfondo", "Cerca", "Risoluzione video"
//
$(document).on("click", "#tipoSfondo button, #rapportoImmagini button, #cerca button, #risoluzioneVideo button, #offset button", function(){
	calcolaConfig()
});

//
// ## Change checkbox "Mostra anteprima", "Dividi immagini composte"
//
$(document).on("change", "#mostraAnteprima, #dividiImmaginiComposte", function(){
	calcolaConfig()
});


//
// ## Modifica degli input testuali "Lingua" e "Lunghezza max riferimenti"
//
function modificaInput() {
	calcolaConfig()
	// Cerca un nome da mostrare per la lingua
	var linguaCorrispondente = $.grep(lingue, function(e) {
		return e.data == $("#lingua").val()
	})[0]
	if (typeof linguaCorrispondente != "undefined")
		$("#spanLingua").text(linguaCorrispondente.value)
}
$(document).on("keyup", "#lingua, #lunghezzaMaxScrittura", modificaInput)
$(document).on("change", "#lingua, #lunghezzaMaxScrittura", modificaInput)
$(document).on("keyup", "#lingua, #lunghezzaMaxVideo", modificaInput)
$(document).on("change", "#lingua, #lunghezzaMaxVideo", modificaInput)

//
// ## Click sui pulsanti "Settimana corrente"
//
$(document).on("click", ".btnAdunanza", function(){

	config.origine.tipo = "adunanza"
	config.origine.valore = $(this).attr("data-val")
	richiediElaborazione(config)

})

//
// ## Invio sul campo "URL"
//
$(document).on("keyup", "#url", function(e){
	if (e.keyCode == 13)
		// Invio
		$("#btnElabora").click()
})

//
// ## Click sul pulsante "Elabora"
//
$(document).on("click", "#btnElabora", function(){

	if ($("#url").val().indexOf("http") == -1) {
		// Riferimento
		config.origine.tipo = "riferimento"
		config.origine.valore = $("#url").val()
	} else {
		// Articolo
		config.origine.tipo = "url"
		config.origine.valore = $("#url").val()
	}
	richiediElaborazione(config)

})

//
// ## Click sul pulsante "Genera link"
//
$(document).on("click", "#generaLink", function(){

	// Crea la stringa con i parametri della configurazione attuale
	calcolaConfig()
	var configurazioneW = $.param(merge(config, { origine: { tipo: "adunanza", valore: "w", offset: 0 } } ))
	var configurazioneWprossima = $.param(merge(config, { origine: { tipo: "adunanza", valore: "w", offset: 1 } } ))
	var configurazioneVCM = $.param(merge(config, { origine: { tipo: "adunanza", valore: "vcm", offset: 0 } } ))
	var configurazioneVCMprossima = $.param(merge(config, { origine: { tipo: "adunanza", valore: "vcm", offset: 1 } } ))

	console.log(configurazioneW)
	console.log(configurazioneWprossima)
	console.log(configurazioneVCM)
	console.log(configurazioneVCMprossima)

	$("#formLinkW input").val(location.origin + location.pathname + "?" + configurazioneW)
	$("#formLinkWprossima input").val(location.origin + location.pathname + "?" + configurazioneWprossima)
	$("#formLinkVCM input").val(location.origin + location.pathname + "?" + configurazioneVCM)
	$("#formLinkVCMprossima input").val(location.origin + location.pathname + "?" + configurazioneVCMprossima)
	$("#formLinkBackendW input").val(backend + "?" + configurazioneW)
	$("#formLinkBackendWprossima input").val(backend + "?" + configurazioneWprossima)
	$("#formLinkBackendVCM input").val(backend + "?" + configurazioneVCM)
	$("#formLinkBackendVCMprossima input").val(backend + "?" + configurazioneVCMprossima)

	// Istanza clipboard.js
	new Clipboard(".copia")

	// Mostra il modal
	UIkit.modal("#modalLinkConfigurazione", { center: true, bgclose: true }).show()

})

//
// ## Alla modifica dell'URL
//
function urlModificato(){

	if (typeof $("#url").val().split("/")[3] != "undefined")
		if ($("#url").val().split("/")[3].length >= 2){
			config.lingua = $("#url").val().split("/")[3];
			mostraConfig()
			modificaInput()
		}

}
$(document).on("paste", "#url", urlModificato);
$(document).on("change", "#url", urlModificato);
$(document).on("keyup", "#url", urlModificato);

//
// ## Al termine del caricamento della pagina
//
$(document).ready(function(){

	// Configurazione
	// Cerca di caricarla dai cookie
	config = Cookies.getJSON("isbo") || {}
	// Ignora la configurazione nei cookie se è riferita a una versione precedente del fronted
	if (config.versione != versioneFrontend)
		config = {}
	// Imposta i singoli parametri con un valore predefinito se non hanno valore
	config.cerca = config.cerca || [ "illustrazioni", "riferimenti" ]
	config.rapportoImmagini = config.rapportoImmagini || "1.77777778"
	config.sfondo = config.sfondo || "img"
	config.risoluzioneVideo = config.risoluzioneVideo || "480"
	config.lunghezzaMaxRiferimenti = config.lunghezzaMaxRiferimenti || "500"
	config.lunghezzaMaxVideo = config.lunghezzaMaxVideo || "15"
	config.lingua = config.lingua || "it"
	config.origine = config.origine || { offset: 0 }
	config.origine.offset = config.origine.offset || 0
	if (config.mostraAnteprima == null)
		config.mostraAnteprima = false
	if (config.dividiImmaginiComposte == null)
		config.dividiImmaginiComposte = true
	config.versione = versioneFrontend
	// Mostra e salva la configurazione
	mostraConfig()
	calcolaConfig()


	// Rende disponibile il pulsante elabora in base all'input#url
	setInterval(function(){

		// Abilitato / Disabilitato
		if ($("#url").val().length > 0 || elaborazioneInCorso)
			$("#btnElabora").removeAttr("disabled");
		else
			$("#btnElabora").attr("disabled", "");

	}, 100);
	urlModificato();

	// Controlla se ci sono dei parametri nell'URL
	var configurazioneDaUrl = $.deparam(location.search.substr(1))
	if (typeof configurazioneDaUrl.origine != "undefined") {
		// Avvia immediatamente l'elaborazione con la configurazione presente nell'URL
		console.log("### Avvio parametrizzato con configurazione:")
		console.log(configurazioneDaUrl)
		richiediElaborazione(configurazioneDaUrl)
	}

	// Versioni
	$.getJSON(backend + "/versione", function(versione) {
		versioneBackend = versione
		console.log("Versione backend: " + versioneBackend)
		console.log("Versione frontend: " + versioneFrontend)
		$("#versioneBackend").text(versioneBackend)
	})

	// IE?
	if (navigator.userAgent.indexOf("MSIE") != -1)
		UIkit.notify('<i class="uk-icon-exclamation-circle"></i>&nbsp;Stai usando una versione non supportata di Internet Explorer. Aggiorna Internet Explorer alla <a target="_blank" href="https://support.microsoft.com/it-it/help/18520/download-internet-explorer-11-offline-installer">versione 11</a> o (meglio ancora) passa a <a target="_blank" href="https://www.google.it/chrome">Chrome</a> o <a target="_blank" href="https://www.mozilla.org/it/firefox/new">Firefox</a>.', "warning", 6000);

	// Carica lingue
	caricaLingue()

	// Url di default (solo sviluppo)
	if (self.urlDefault)
		$("#url").val(self.urlDefault)


});

//
// ## Click sul pulsante "Indietro" in un modal
//
$(document).on("click", ".btnChiudiModal", function(){
	$.UIkit.modal($(this).closest(".uk-modal")).hide()
});

//
// ## Al caricamento di una o più immagini
//
$(document).on("change", "#fileUpload", function(){

	// Controlla anche che i file siano delle immagini
	var mimetypeValidi = [ "image/jpeg", "image/jpg", "image/bmp", "image/png" ]
	var tuttiFileValidi = true

	var form = new FormData()
	$.each($("#fileUpload")[0].files, function (i, file) {

		if (mimetypeValidi.indexOf(file.type) == -1)
			tuttiFileValidi = false

		// Tutto ok, aggiunge il file
		form.append("immagini", file)

	})

	if (tuttiFileValidi) {

		// Invia solo i parametri necessari
		form.append("rapportoImmagini", config.rapportoImmagini)
		form.append("dividiImmaginiComposte", config.dividiImmaginiComposte)

		richiediElaborazione(form)

	} else
		mostraErrore("I file devono essere di tipo immagine")


});
