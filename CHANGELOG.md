# 1.4.3 (03/03/2018)
 * Nuovo versioning (major.minr.patch)
 * Adattato `stile.less` a sintassi Less più recente (estensioni .less esplicite)

# 1.4.2 (06/12/2017)
 * Nuovo parametro "dividi immagini composte"

# 1.4.1 (05/12/2017)
 * Nuovo parametro "lunghezza massima video"

# 1.4.0 (04/12/2017)
 * Migliore gestione errori backend (Unicode)
 * Antemprima dei contenuti ottenuti nel browser

# 1.3.3 (12/10/2017)
 * Redirect automatico HTTP->HTTPS

# 1.3.2 (04/10/2017)
 * Passato al nuovo backend (HTTPS)

# 1.3.1 (08/08/2017)
 * Migliorie varia alla GUI

# 1.3.0 (01/08/2017)
 * Prima release con webpack

# 1.2.0 (07/07/2017)
 * Implementato upload immagini locali
 * Migliorie varie alla GUI

# 1.1.1 (11/03/2017)
 * Cambiato indirizzo backend (AWS)

# 1.1.0 (21/02/2017)
 * Autocompletamento lingue
