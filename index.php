<?php

	// Versione frontend
	$versioneFrontend = explode(" ", trim(file("CHANGELOG.md")[0]))[1];
?>
<!DOCTYPE html>
<html lang="it-it" dir="ltr" class="uk-height-1-1">
	<head>
		<meta charset="UTF-8">

		<title>Contenuti multimediali dalla Biblioteca Online</title>

		<link rel="icon" href="img/favicon.ico" type="image/x-icon">

		<?php if ($_SERVER["HTTP_HOST"] != "localhost") { ?>
			<script>
				(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
				})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
				ga('create', 'UA-86970983-1', 'auto');
				ga('send', 'pageview');
			</script>
		<?php } ?>

		<script src="dist/script.bundle.js"></script>

    </head>

	<body class="uk-flex uk-flex-center">

		<div id="divMain" class="uk-margin-large-top">
			<h2 class="uk-flex uk-flex-middle">
				<div><img class="uk-icon-button uk-margin-right" src="img/wt-144.png"></div>
				<div>Contenuti multimediali dalla Biblioteca Online</div>
			</h2>

			<div id="divOrigineDati" class="uk-panel uk-panel-box uk-panel-box-secondary uk-animation-fade uk-form uk-form-horizontal">
				<fieldset>
					<legend>Origine dati</legend>
					<div class="uk-margin-bottom">
						<div class="uk-flex uk-flex-middle">
							<div class="uk-badge uk-badge-notification uk-margin-right">1</div>
							<div>Inserisci un URL di una pagina della Biblioteca Online per ottenere tutte le illustrazioni e le scritture dell&apos;articolo.</div>
						</div>
						<div class="uk-flex uk-flex-middle uk-margin-top">
							<div class="uk-badge uk-badge-notification uk-margin-right">2</div>
							<div>Inserisci un riferimento a una scrittura (es.: <i>Matteo 24:14</i>) per ottenerne l&apos;immagine.</div>
						</div>
					</div>
					<div class="uk-form-row">
						<div class="uk-form-icon uk-width-1-1">
							<i class="uk-icon-link"></i>
							<i class="uk-icon-circle-o-notch uk-icon-spin uk-hidden"></i>
							<input id="url" value="" class="uk-width-1-1 uk-form-large" type="text" placeholder="URL dell'articolo sulla Biblioteca Online o riferimento a una scrittura">
						</div>
					</div>
					<div class="uk-flex uk-flex-middle uk-margin">
						<div class="uk-badge uk-badge-notification uk-margin-right">3</div>
						<div>Oppure imposta la lingua e seleziona un tipo di adunanza.</div>
					</div>
					<div class="uk-form-row">
						<label class="uk-form-label">
							<i class="uk-icon-language uk-icon-justify uk-margin-small-left uk-margin-small-right"></i>
							Lingua:
						</label>
						<div class="uk-form-controls">
							<input id="lingua" class="uk-form-width-small uk-text-primary" type="text" value="">
							<span class="uk-text-muted uk-margin-left" id="spanLingua"></span>
						</div>
					</div>
					<div class="uk-form-row">
						<label class="uk-form-label uk-margin-right uk-text-nowrap">
							<i class="uk-icon-calendar uk-icon-justify uk-margin-small-left uk-margin-small-right"></i>
							Settimana
						</label>
						<div class="uk-form-controls" data-uk-margin>
							<div id="offset" class="uk-button-group" data-uk-button-radio>
								<button type="button" data-val="0" class="uk-button uk-button-small">Corrente</button>
								<button type="button" data-val="1" class="uk-button uk-button-small">Prossima</button>
							</div>
						</div>
					</div>
					<div class="uk-form-row">
						<label class="uk-form-label uk-margin-right uk-text-nowrap">
							<i class="uk-icon-book uk-icon-justify uk-margin-small-left uk-margin-small-right"></i>
							Adunanza
						</label>
						<div class="uk-form-controls" data-uk-margin>
							<button type="button" class="btnAdunanza uk-button uk-button-success" data-val="w"><i class="isbo-w uk-icon-justify uk-margin-small-right"></i>Torre di Guardia</button>
							<button type="button" class="btnAdunanza uk-button uk-button-success" data-val="vcm"><i class="isbo-vcm uk-icon-justify uk-margin-small-right"></i>Vita cristiana e ministero</button>
						</div>
					</div>
					<div class="uk-flex uk-flex-middle uk-margin">
						<div class="uk-badge uk-badge-notification uk-margin-right">4</div>
						<div>Puoi anche selezionare delle immagini dal tuo dispositivo.</div>
					</div>
					<div class="uk-form-row">
						<div id="uploadDrop" class="uk-placeholder uk-text-center">
							<div>
								<i class="uk-icon-cloud-upload uk-icon-medium uk-text-muted uk-margin-small-right"></i>
								<span>Trascina qui dei file di tipo immagine o fai click per selezionarli.</span>
							</div>
							<input type="file" id="fileUpload" multiple>
						</div>
					</div>
				</fieldset>
				<fieldset class="uk-margin-top">
					<legend>Configura</legend>
					<div class="uk-form-row">
						<label class="uk-form-label uk-margin-right">
							<i class="uk-icon-cogs uk-icon-justify uk-margin-small-left uk-margin-small-right"></i>
							Cerca:
						</label>
						<div class="uk-form-controls">
							<div id="cerca" class="uk-button-group" data-uk-button-checkbox>
								<button type="button" data-val="illustrazioni" class="uk-button uk-button-small">Illustrazioni</button>
								<button type="button" data-val="riferimenti" class="uk-button uk-button-small">Riferimenti</button>
								<button type="button" data-val="video" class="uk-button uk-button-small">Video</button>
								<button type="button" data-val="link" class="uk-button uk-button-small">Link</button>
							</div>
						</div>
					</div>
					<div class="uk-form-row">
						<label class="uk-form-label uk-margin-right">
							<i class="uk-icon-expand uk-icon-justify uk-margin-small-left uk-margin-small-right"></i>
							Rapporto immagini
						</label>
						<div class="uk-form-controls">
							<div id="rapportoImmagini" class="uk-button-group" data-uk-button-radio>
								<button type="button" data-val="1.77777778" class="uk-button uk-button-small">16:9</button>
								<button type="button" data-val="1.6" class="uk-button uk-button-small">16:10</button>
								<button type="button" data-val="1.33333333" class="uk-button uk-button-small">4:3</button>
								<button type="button" data-val="1.66666667" class="uk-button uk-button-small">5:3</button>
							</div>
						</div>
					</div>
					<div class="uk-form-row uk-animation-fade">
						<label class="uk-form-label uk-margin-right">
							<i class="uk-icon-video-camera uk-icon-justify uk-margin-small-left uk-margin-small-right"></i>
							Risoluzione video
						</label>
						<div class="uk-form-controls">
							<div id="risoluzioneVideo" class="uk-button-group" data-uk-button-radio>
								<button type="button" data-val="240" class="uk-button uk-button-small">240p</button>
								<button type="button" data-val="360" class="uk-button uk-button-small">360p</button>
								<button type="button" data-val="480" class="uk-button uk-button-small">480p</button>
								<button type="button" data-val="720" class="uk-button uk-button-small">720p</button>
							</div>
						</div>
					</div>
					<div class="uk-form-row uk-animation-fade">
						<label class="uk-form-label">
							<i class="uk-icon-file-video-o uk-icon-justify uk-margin-small-left uk-margin-small-right"></i>
							Ignora video pi&ugrave; lunghi di
						</label>
						<div class="uk-form-controls">
							<input id="lunghezzaMaxVideo" class="uk-form-width-mini uk-text-center uk-text-primary" type="text" value="">
							<span>minuti</span>
						</div>
					</div>
					<div class="uk-form-row uk-animation-fade">
						<label class="uk-form-label uk-margin-right">
							<i class="uk-icon-paint-brush uk-icon-justify uk-margin-small-left uk-margin-small-right"></i>
							Sfondo scritture
						</label>
						<div class="uk-form-controls">
							<div id="tipoSfondo" class="uk-button-group" data-uk-button-radio>
								<button type="button" data-val="img" class="uk-button uk-button-small">Immagine</button>
								<button type="button" data-val="nero" class="uk-button uk-button-small">Nero</button>
							</div>
						</div>
					</div>
					<div class="uk-form-row uk-animation-fade">
						<label class="uk-form-label">
							<i class="uk-icon-font uk-icon-justify uk-margin-small-left uk-margin-small-right"></i>
							Ignora scritture con pi&ugrave; di
						</label>
						<div class="uk-form-controls">
							<input id="lunghezzaMaxScrittura" class="uk-form-width-mini uk-text-center uk-text-primary" type="text" value="">
							<span>caratteri</span>
						</div>
					</div>
					<div class="uk-form-row uk-animation-fade">
						<label class="uk-form-label">
							<i class="uk-icon-crop uk-icon-justify uk-margin-small-left uk-margin-small-right"></i>
							Avanzate
						</label>
						<div class="uk-form-controls uk-form-controls-text">
							<label><input id="dividiImmaginiComposte" type="checkbox"> Dividi immagini composte</label>
						</div>
					</div>
				</fieldset>
				<fieldset class="uk-margin-top">
					<legend>Elabora</legend>
					<div class="uk-form-row">
						<label class="uk-form-label uk-margin-right">
							<i class="uk-icon-download uk-icon-justify uk-margin-small-left uk-margin-small-right"></i>
							Varie
						</label>
						<div class="uk-form-controls uk-form-controls-text">
							<label><input id="mostraAnteprima" type="checkbox"> Mostra contenuti prima di scaricarli</label>
						</div>
					</div>
					<div class="uk-form-row">
						<button id="btnElabora" type="button" class="uk-width-1-1 uk-button uk-button-success uk-button-large uk-margin-bottom uk-flex uk-flex-center">
							<div>
								<i class="uk-icon-cog"></i>
								<span>Elabora</span>
							</div>
							<div class="uk-margin-large-left uk-hidden">
								<i class="uk-icon-clock-o"></i>
								<span id="spanEta"></span>
							</div>
						</button>
						<div class="uk-progress uk-progress-striped uk-invisible uk-margin-remove">
							<div class="uk-progress-bar" style="width: 0%;"></div>
						</div>
					</div>
				</fieldset>
			</div>

			<div class="uk-text-muted uk-text-small uk-text-center uk-margin-top uk-margin-bottom">
				<a id="generaLink" class="uk-text-muted" >Genera link</a>
				&nbsp;- Versione frontend <?php echo $versioneFrontend; ?> (<a class="uk-text-muted" target="_blank" href="https://gitlab.com/davideblasutto/ISBOFrontend"><i class="uk-icon-gitlab"></i> Sorgente</a>) - Versione backend <span id="versioneBackend">...</span> (<a class="uk-text-muted" target="_blank" href="https://gitlab.com/davideblasutto/ISBOBackend"><i class="uk-icon-gitlab"></i> Sorgente</a>)
				<script>
					versioneFrontend = "<?php echo $versioneFrontend; ?>"
				</script>
			</div>

		</div>

		<!-- MODAL ANTEPRIMA CONTENUTI -->
		<div id="modalAnteprima" class="uk-modal">
		    <div class="uk-modal-dialog uk-modal-dialog-large">
		        <a class="uk-modal-close uk-close"></a>
				<div class="uk-modal-header">
					<h2>
						<i class="uk-icon-picture-o uk-margin-small-right"></i>
						Anteprima contenuti
					</h2>
				</div>
				<div class="uk-overflow-container">
					<div id="containerAnteprima" class="uk-grid uk-margin-right" data-uk-grid data-uk-grid-match data-uk-grid-margin> </div>
				</div>
				<div class="uk-modal-footer uk-flex uk-flex-center">
					<button id="btnScarica" type="button" class="uk-button uk-button-success uk-button-large">
							<i class="uk-icon-download uk-margin-small-right"></i>Scarica
					</button>
				</div>
		    </div>
		</div>

		<!-- MODAL LINK ELABORAZIONE DIRETTA -->
		<div id="modalLinkConfigurazione" class="uk-modal">
			<div class="uk-modal-dialog" >
		        <a class="uk-modal-close uk-close"></a>
				<div class="uk-modal-header">
					<h2>
						<i class="uk-icon-cogs uk-margin-small-right"></i>
						Elaborazione immediata
					</h2>
				</div>

				<div class="uk-overflow-container">
					<div class="uk-margin-right">
						<h3>
							<i class="uk-icon-globe uk-margin-small-right uk-icon-justify"></i>
							Via browser
						</h3>
						<div>
							Aprendo questi uno di questi link verrà avviata immediatamente l'elaborazione dei contenuti per l'adunanza con la configurazione attuale:
						</div>
						<h4 class="uk-text-muted">Settimana corrente</h3>
						<div id="formLinkVCM" class="uk-form uk-grid uk-grid-small uk-margin-top">
							<div class="uk-width-1-4 uk-flex uk-flex-middle">
								<i class="isbo-vcm uk-margin-small-right uk-icon-justify"></i>
								Vita cristinana
							</div>
							<div class="uk-width-2-4">
								<input type="text" id="linkVCM" class="uk-width-1-1">
							</div>
							<div class="uk-width-1-4">
							<button class="uk-button copiaLink uk-width-1-1 copia" type="button" data-clipboard-target="#linkVCM">
								<i class="uk-icon-clipboard uk-margin-small-right uk-icon-justify"></i>
								Copia
							</button>
							</div>
						</div>
						<div id="formLinkW" class="uk-form uk-grid uk-grid-small uk-margin-top">
							<div class="uk-width-1-4 uk-flex uk-flex-middle">
								<i class="isbo-w uk-margin-small-right uk-icon-justify"></i>
								Torre di Guardia
							</div>
							<div class="uk-width-2-4">
								<input type="text" id="linkW" class="uk-width-1-1">
							</div>
							<div class="uk-width-1-4">
							<button class="uk-button copiaLink uk-width-1-1 copia" type="button" data-clipboard-target="#linkW">
								<i class="uk-icon-clipboard uk-margin-small-right uk-icon-justify"></i>
								Copia
							</button>
							</div>
						</div>
						<h4 class="uk-text-muted">Settimana prossima</h3>
						<div id="formLinkVCMprossima" class="uk-form uk-grid uk-grid-small uk-margin-top">
							<div class="uk-width-1-4 uk-flex uk-flex-middle">
								<i class="isbo-vcm uk-margin-small-right uk-icon-justify"></i>
								Vita cristinana
							</div>
							<div class="uk-width-2-4">
								<input type="text" id="linkVCMprossima" class="uk-width-1-1">
							</div>
							<div class="uk-width-1-4">
							<button class="uk-button copiaLink uk-width-1-1 copia" type="button" data-clipboard-target="#linkVCMprossima">
								<i class="uk-icon-clipboard uk-margin-small-right uk-icon-justify"></i>
								Copia
							</button>
							</div>
						</div>
						<div id="formLinkWprossima" class="uk-form uk-grid uk-grid-small uk-margin-top">
							<div class="uk-width-1-4 uk-flex uk-flex-middle">
								<i class="isbo-w uk-margin-small-right uk-icon-justify"></i>
								Torre di Guardia
							</div>
							<div class="uk-width-2-4">
								<input type="text" id="linkWprossima" class="uk-width-1-1">
							</div>
							<div class="uk-width-1-4">
							<button class="uk-button copiaLink uk-width-1-1 copia" type="button" data-clipboard-target="#linkWprossima">
								<i class="uk-icon-clipboard uk-margin-small-right uk-icon-justify"></i>
								Copia
							</button>
							</div>
						</div>
						<hr>
						<h3>
							<i class="uk-icon-file-code-o uk-margin-small-right uk-icon-justify"></i>
							Via HTTP
						</h3>
						<div>
							Puoi anche inviare la richiesta direttamente al backend, che risponderà restituendo l'archivio con i contenuti o un messaggio di testo in caso di errore.
						</div>
						<h4 class="uk-text-muted">Settimana corrente</h3>
						<div id="formLinkBackendVCM" class="uk-form uk-grid uk-grid-small uk-margin-top">
							<div class="uk-width-1-4 uk-flex uk-flex-middle">
								<i class="isbo-vcm uk-margin-small-right uk-icon-justify"></i>
								Vita cristinana
							</div>
							<div class="uk-width-2-4">
								<input type="text" id="linkBackendVCM" class="uk-width-1-1">
							</div>
							<div class="uk-width-1-4">
							<button class="uk-button copiaLink uk-width-1-1 copia" type="button" data-clipboard-target="#linkBackendVCM">
								<i class="uk-icon-clipboard uk-margin-small-right uk-icon-justify"></i>
								Copia
							</button>
							</div>
						</div>
						<div id="formLinkBackendW" class="uk-form uk-grid uk-grid-small uk-margin-top">
							<div class="uk-width-1-4 uk-flex uk-flex-middle">
								<i class="isbo-w uk-margin-small-right uk-icon-justify"></i>
								Torre di Guardia
							</div>
							<div class="uk-width-2-4">
								<input type="text" id="linkBackendW" class="uk-width-1-1">
							</div>
							<div class="uk-width-1-4">
							<button class="uk-button copiaLink uk-width-1-1 copia" type="button" data-clipboard-target="#linkBackendW">
								<i class="uk-icon-clipboard uk-margin-small-right uk-icon-justify"></i>
								Copia
							</button>
							</div>
						</div>
						<h4 class="uk-text-muted">Settimana prossima</h3>
						<div id="formLinkBackendVCMprossima" class="uk-form uk-grid uk-grid-small uk-margin-top">
							<div class="uk-width-1-4 uk-flex uk-flex-middle">
								<i class="isbo-vcm uk-margin-small-right uk-icon-justify"></i>
								Vita cristinana
							</div>
							<div class="uk-width-2-4">
								<input type="text" id="linkBackendVCMprossima" class="uk-width-1-1">
							</div>
							<div class="uk-width-1-4">
							<button class="uk-button copiaLink uk-width-1-1 copia" type="button" data-clipboard-target="#linkBackendVCMprossima">
								<i class="uk-icon-clipboard uk-margin-small-right uk-icon-justify"></i>
								Copia
							</button>
							</div>
						</div>
						<div id="formLinkBackendWprossima" class="uk-form uk-grid uk-grid-small uk-margin-top">
							<div class="uk-width-1-4 uk-flex uk-flex-middle">
								<i class="isbo-w uk-margin-small-right uk-icon-justify"></i>
								Torre di Guardia
							</div>
							<div class="uk-width-2-4">
								<input type="text" id="linkBackendWprossima" class="uk-width-1-1">
							</div>
							<div class="uk-width-1-4">
							<button class="uk-button copiaLink uk-width-1-1 copia" type="button" data-clipboard-target="#linkBackendWprossima">
								<i class="uk-icon-clipboard uk-margin-small-right uk-icon-justify"></i>
								Copia
							</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- TEMPLATE ANTEPRIMA SINGOLA IMMAGINE -->

		<div id="templateAnteprimaImmagine" class="elemento uk-width-large-1-3 uk-width-medium-1-2 uk-hidden">
			<div class="uk-panel uk-panel-box">
				<h3 class="uk-panel-title uk-text-truncate">
					<i class="uk-icon-file-picture-o uk-margin-small-right uk-text-muted"></i>
					<span></span>
				</h3>
				<img>
			</div>
		</div>

		<div id="templateAnteprimaLink" class="elemento uk-width-large-1-3 uk-width-medium-1-2 uk-hidden">
			<div class="uk-panel uk-panel-box">
				<h3 class="uk-panel-title uk-text-truncate">
					<i class="uk-icon-link uk-margin-small-right uk-text-muted"></i>
					<span></span>
				</h3>
				<div class="caricamento uk-flex uk-flex-middle uk-flex-center uk-text-large">
					<i class="uk-icon-circle-o-notch uk-icon-spin"></i>
				</div>
				<img class="uk-hidden">
			</div>
		</div>

		<div id="templateAnteprimaVideo" class="elemento uk-width-large-1-3 uk-width-medium-1-2 uk-hidden">
			<div class="uk-panel uk-panel-box">
				<h3 class="uk-panel-title uk-text-truncate">
					<i class="uk-icon-file-video-o uk-margin-small-right uk-text-muted"></i>
					<span></span>
				</h3>
				<div class="uk-flex uk-flex-middle uk-flex-center uk-width-1-1">
					<img>
				</div>
			</div>
		</div>

	</body>
</html>
