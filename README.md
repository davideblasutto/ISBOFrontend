# Illustrazioni, scritture e link dalla Biblioteca Online #
# Frontend #

Estrae illustrazioni, scritture e link da un articolo di wol.jw.org.
Permette anche di caricare ed elaborare immagini secondo le stesse regole (ridimensionamento e margini blurrati).

Accessibile da http://isbo.herokuapp.com.

## Browser supportati #
* Firefox
* Chrome
* Edge
* IE 11
