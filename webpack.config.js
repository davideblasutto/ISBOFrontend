var webpack = require('webpack')

module.exports = {
	entry: [ 'babel-polyfill', './src/script.js' ],
	output: {
		filename: './dist/script.bundle.js'
	},
	module: {
		rules: [{
      		test: /\.js$/,
			exclude: /(node_modules|bower_components)/,
			use: { loader: 'babel-loader', options: { presets: ['@babel/preset-env'] } }
		},{
			// Caricamento Less
            test: /\.less$/,
            use: [ 'style-loader', 'css-loader', 'less-loader' ]
        }, {
			// Caricamento file binari
			test: /\.(png|woff|woff2|eot|ttf|svg)$/,
			use: [ 'file-loader?name=dist/[name].[ext]' ]
		}]
	},
	// Usa versioni non minificate
	resolve: {
        alias: {
            jquery: 'jquery/src/jquery'
        }
    },
	plugins: [
		// Rende jQuery accessibile ovunque
		new webpack.ProvidePlugin({
			$: 'jquery',
			jQuery: 'jquery',
			'window.jQuery': 'jquery'
		})
	],
	node: {
		// Evita problemi con jsmediatags, che sembra richiedere fs
  		fs: 'empty'
	}
}
